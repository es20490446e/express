#! /bin/bash

post_install () {
	UpdateIconCache
}


post_upgrade () {
	UpdateIconCache
}


post_remove () {
	UpdateIconCache
}


UpdateIconCache () {
	gtk-update-icon-cache --force --quiet --ignore-theme-index "usr/share/icons/nuoveXT23"
}
