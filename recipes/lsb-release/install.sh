#! /bin/bash


post_install () {
	if [[ ! -f "/etc/lsb_release" ]] && pacman --sync --info archlinux-release &> /dev/null; then
		rm "/var/lib/pacman/db.lck"
		pacman --sync --asdep "archlinux-release"
		touch "/var/lib/pacman/db.lck"
	fi
}


post_upgrade () {
	post_install
}
