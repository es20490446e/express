#! /bin/bash


post_install () {
	EnableServices
}


post_upgrade () {
	ReenableServices
}


pre_remove () {
	DisableServices
}


DisableServices () {
	DisableUserService
	DisableSystemService
}


DisableSystemService () {
	systemctl disable bluetooth-autoconnect.service
}


DisableUserService () {
	systemctl disable --user --global pipewire-bluetooth-autoconnect
	systemctl disable --user --global pulseaudio-bluetooth-autoconnect
}


EnableServices () {
	EnableSystemService
	EnableUserService
}


EnableSystemService () {
	systemctl enable bluetooth-autoconnect.service
}


EnableUserService () {
	if [[ -x "/usr/bin/pipewire-pulse" ]]; then
		systemctl enable --user --global pipewire-bluetooth-autoconnect.service
	elif [[ -x "/usr/bin/pulseaudio" ]]; then
		systemctl enable --user --global pulseaudio-bluetooth-autoconnect.service
	fi
}


ReenableServices () {
	DisableServices
	EnableServices
}
