#! /bin/bash


pre_install () {
	rm --force /etc/sysctl.d/99-vm-zram-parameters.conf
}


post_install () {
	systemctl restart systemd-sysctl
}


post_upgrade () {
	systemctl restart systemd-sysctl
}


post_remove () {
	systemctl restart systemd-sysctl
}
