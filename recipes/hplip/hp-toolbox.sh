#! /bin/bash


mainFunction () {
	cd "/usr/share/hplip"
	./systray.py &
	trap 'terminateChildren' ERR INT TERM QUIT EXIT
	systray=$!
	./toolbox.py
}


terminateChildren () {
	pkill -TERM -P "${systray}" &>/dev/null || true
	pkill -TERM -P $$ &>/dev/null || true
}


mainFunction
