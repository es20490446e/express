#! /bin/bash

pkgname="ventoy"
install="install.sh"

epoch=100
pkgrel=1
arch=("x86_64")

pkgdesc="Makes storage that can boot OS images"
url="https://github.com/ventoy/Ventoy"
license=("GPL")

makedepends=("so")
depends=("dosfstools")


pkgver="$(
	repoInfo tags "${url}" |
	tr -d 'v' |
	head -n1
)"


source=(
	"${url}/releases/download/v${pkgver}/ventoy-${pkgver}-linux.tar.gz"
	"ventoy.desktop"
	"sanitize.patch"
)


sha1sums=(); readarray -t sha1sums < <(
	for Item in "${source[@]}"; do
		echo "SKIP"
	done
)


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


prepare () {
	DecompressTools
	Sanitize
	RemoveUnusedBinaries
}


DecompressTools () {
	local File
	cd "${srcdir}/${pkgname}-${pkgver}/tool/x86_64"

	for File in *.xz; do
		xzcat "${File}" > "${File%.xz}"
		so chmod +x "${File%.xz}"
	done

	so rm --force ./*.xz
}


Sanitize () {
	cd "${srcdir}/${pkgname}-${pkgver}"

	so patch --strip=0 < "${srcdir}/sanitize.patch"
	so sed --in-place "s|log\.txt|/var/log/ventoy.log|g" "WebUI/static/js/languages.js" "tool/languages.json"
	so sed --in-place "s|bin/sh|usr/bin/env bash|g" "tool/"{ventoy_lib.sh,VentoyWorker.sh}
}


RemoveUnusedBinaries () {
	local Binary
	cd "${srcdir}/${pkgname}-${pkgver}"

	for Binary in xzcat hexdump; do
		so rm --force "tool/x86_64/${Binary}"
	done
}


package () {
	PackageFiles
	LinkSystemTools
	MakeBinDir
}


PackageFiles () {
	cd "${srcdir}/${pkgname}-${pkgver}"

	so install -Dm755 -t "${pkgdir}/opt/ventoy/" "VentoyGUI.x86_64"
	so install -Dm755 -t "${pkgdir}/opt/ventoy/" ./*.sh
	so install -Dm644 -t "${pkgdir}/opt/ventoy/boot/" boot/*
	so install -Dm755 -t "${pkgdir}/opt/ventoy/tool/" tool/*.{cer,glade,json,sh,xz}
	so install -Dm755 -t "${pkgdir}/opt/ventoy/tool/x86_64/" tool/x86_64/*
	so install -Dm644 -t "${pkgdir}/opt/ventoy/ventoy/" ventoy/*
	so install -Dm644 -t "${pkgdir}/usr/share/applications" "${srcdir}/ventoy.desktop"

	so rm "${pkgdir}/opt/ventoy/tool/x86_64/Ventoy2Disk.gtk2"

	so cp --archive --no-preserve=o --target-directory="${pkgdir}/opt/ventoy/" "plugin" "WebUI"
	so install -Dm644 "WebUI/static/img/VentoyLogo.png" "${pkgdir}/usr/share/pixmaps/ventoy.png"
}


LinkSystemTools () {
	local Binary
	cd "${srcdir}/${pkgname}-${pkgver}"

	for Binary in xzcat hexdump; do
		so ln --symbolic "/usr/bin/${Binary}" "${pkgdir}/opt/ventoy/tool/x86_64/"
	done
}


MakeBinDir () {
	so mkdir --parents "${pkgdir}/usr/bin"
	MakeBinLinks
	MakeOptFiles
}


MakeBinLinks () {
	so ln --symbolic "/opt/ventoy/ExtendPersistentImg.sh" "${pkgdir}/usr/bin/ventoy-extend-persistent"
	so ln --symbolic "/opt/ventoy/CreatePersistentImg.sh" "${pkgdir}/usr/bin/ventoy-persistent"
}


MakeOptFiles () {
	MakeOptFile "ventoy" "Ventoy2Disk.sh"
	MakeOptFile "ventoygui" "VentoyGUI.x86_64"
	MakeOptFile "ventoyplugson" "VentoyPlugson.sh"
	MakeOptFile "ventoyweb" "VentoyWeb.sh"
}


MakeOptFile () {
	local Name="${1}"
	local Target="${2}"

	{
		echo '#! /bin/bash'
		echo 'set -e'
		echo
		echo 'cd /opt/ventoy'
		printf 'exec ./%s "$@"\n' "${Target}"
	} > "${pkgdir}/usr/bin/${Name}"

	so chmod +x "${pkgdir}/usr/bin/${Name}"
}
