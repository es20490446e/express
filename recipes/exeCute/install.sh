#! /bin/bash


post_install () {
	systemctl restart systemd-binfmt
}


post_remove () {
	post_install
}
