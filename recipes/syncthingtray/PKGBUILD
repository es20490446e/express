#! /bin/bash

pkgname="syncthingtray"

epoch=100
pkgrel=1
arch=("x86_64")

pkgdesc="Syncs files between devices on the same network"
url="https://github.com/Martchus/syncthingtray"
license=("GPL")


pkgver="$(
	repoInfo tags "${url}" |
	cut --delimiter='v' --fields=2- |
	head -n1
)"


QtVersion="$(
	pacman --sync --info qtforkawesome |
	grep 'Version' |
	cut --delimiter=':' --fields=2- |
	tr -d ' ' |
	rev |
	cut --delimiter=':' --fields=1 |
	rev |
	cut --delimiter='t' --fields=2 |
	cut --delimiter='v' --fields=1
)"


source=("git+${url}.git#tag=v${pkgver}")
sha1sums=("SKIP")


makedepends=(
	"boost"
	"clang"
	"cmake"
	"extra-cmake-modules"
	"git"
	"kio"
	"libplasma"
	"ninja"
	"qt${QtVersion}-tools"
	"qt${QtVersion}-declarative"
)


checkdepends=(
	"cppunit"
	"iproute2"
	"syncthing"
)


depends=(
	"boost-libs"
	"c++utilities"
	"desktop-file-utils"
	"openssl"
	"syncthing"
	"qt${QtVersion}-declarative"
	"qt${QtVersion}-svg"
	"qtforkawesome"
	"qtutilities"
)


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


build () {
	if [[ ! -f "${srcdir}/build.lock" ]]; then
		EnterBuildDir
		ConfigureBuild
		so ninja
	fi
}


EnterBuildDir () {
	so rm --recursive --force "${srcdir}/build"
	so cp --archive "${srcdir}/${pkgname}" "${srcdir}/build"
	cd "${srcdir}/build"
}


ConfigureBuild () {
	so cmake \
		-G Ninja \
		-D BUILD_SHARED_LIBS:BOOL=ON \
		-D BUILTIN_TRANSLATIONS:BOOL=ON \
		-D BUILTIN_TRANSLATIONS_OF_QT:BOOL=OFF \
		-D CMAKE_BUILD_TYPE:STRING="Release" \
		-D CMAKE_INSTALL_PREFIX:PATH="/usr" \
		-D CONFIGURATION_PACKAGE_SUFFIX="-$(CppUtilitiesMajorVersion)" \
		-D CONFIGURATION_PACKAGE_SUFFIX_QTUTILITIES:STRING="-${QtVersion}" \
		-D JS_PROVIDER=qml \
		-D KF_PACKAGE_PREFIX:STRING="KF${QtVersion}" \
		-D QT_PACKAGE_PREFIX:STRING="Qt${QtVersion}" \
		-D SYSTEMD_SUPPORT=ON \
		-D WEBVIEW_PROVIDER=none \
		.
}


CppUtilitiesMajorVersion () {
	pacman --sync --print-format "%v" c++utilities |
	rev |
	cut --delimiter=':' --fields=1 |
	rev |
	cut --delimiter='.' --fields=1
}


check () {
	cd "${srcdir}/build"

	# https://github.com/syncthing/syncthing/issues/8785
	HOME="$(mktemp -p . -d "testhome.XXX")" \
	QT_QPA_PLATFORM=offscreen \
	SYNCTHING_PORT=$(EphemeralPort) \
	SYNCTHING_TEST_TIMEOUT_FACTOR=5 \
	so ninja check

	touch "${srcdir}/build.lock"
}


EphemeralPort () {
	comm -2 -3 <(seq 49152 65535) <(
		ss --all --numeric --tcp |
		awk '{print $4}' |
		cut --delimiter=':' --fields=2 |
		grep "[0-9]\{1,5\}" |
		sort --unique
	) |
	shuf |
	head -n1
}


package () {
	cd "$srcdir/build"
	DESTDIR="${pkgdir}" so ninja install
	source "/usr/lib/AppendLibs"
}
