#! /bin/bash


post_install () {
	so systemctl enable locale-menu.service
}


post_upgrade () {
	so systemctl --system daemon-reload
}


pre_remove () {
	so systemctl disable locale-menu.service
}


post_remove () {
	so systemctl --system daemon-reload
}
