#! /bin/bash

Conf="etc/pacman.conf"


pre_install () {
	EnableSockets
}


pre_upgrade () {
	EnableSockets
	ChangeManjaroBranch
	RemoveSyncFirstOption
	AddSandboxOptions
}


ChangeManjaroBranch () {
	if [[ -x "usr/bin/pacman-mirrors" ]] && [[ "$(pacman-mirrors --get-branch)" != "unstable" ]]; then
		pacman-mirrors --api --set-branch unstable --fasttrack 10
	fi
}


EnableSockets () {
	local WantsDir="$pkgdir/usr/lib/systemd/system/sockets.target.wants"
	install -dm755 "${WantsDir}"

	local Unit
	local Units=(
		dirmngr
		gpg-agent
		gpg-agent-browser
		gpg-agent-extra
		gpg-agent-ssh
		keyboxd
	)

	for Unit in "${Units[@]}"; do
		local Link="${WantsDir}/${Unit}@etc-pacman.d-gnupg.socket"

		if [[ ! -L "${Link}" ]]; then
			ln --symbolic "../${Unit}@.socket" "${Link}"
		fi
	done
}


RemoveSyncFirstOption () {
	if [[ -f "${Conf}" ]]; then
		sed --in-place '/SyncFirst/d' "${Conf}"
	fi
}


AddSandboxOptions () {
	if [[ -f "${Conf}" ]] &&
	! grep --quiet "DownloadUser" "${Conf}" &&
	! grep --quiet "DisableSandbox" "${Conf}" &&
	[[ "$(stat --format=%Y "${Conf}")" -lt 1728639917 ]]; then
		sed --in-place 's|CheckSpace|\nCheckSpace\nDisableSandbox|' "${Conf}"
	fi
}
