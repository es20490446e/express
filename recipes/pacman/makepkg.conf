#! /hint/bash
#  shellcheck disable=2034
#  help at: man makepkg.conf


### DOWNLOADS

DLAGENTS=(
	'file::/usr/bin/curl -qgC - -o %o %u'
	'ftp::/usr/bin/curl -qgfC - --ftp-pasv --retry 3 --retry-delay 3 -o %o %u'
	'http::/usr/bin/curl -qgb "" -fLC - --retry 3 --retry-delay 3 -o %o %u'
	'https::/usr/bin/curl -qgb "" -fLC - --retry 3 --retry-delay 3 -o %o %u'
	'rsync::/usr/bin/rsync --no-motd -z %u %o'
	'scp::/usr/bin/scp -C %u %o'
)

VCSCLIENTS=(
	'bzr::breezy'
	'fossil::fossil'
	'git::git'
	'hg::mercurial'
	'svn::subversion'
)


### FLAGS

CARCH="x86_64"
CHOST="x86_64-pc-linux-gnu"

CPPFLAGS=""
CFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions \
        -Wp,-D_FORTIFY_SOURCE=3 -Wformat -Werror=format-security \
        -fstack-clash-protection -fcf-protection \
        -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer"
CXXFLAGS="$CFLAGS -Wp,-D_GLIBCXX_ASSERTIONS"
LDFLAGS="-Wl,-O1 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now \
         -Wl,-z,pack-relative-relocs"
LTOFLAGS="-flto=auto"
MAKEFLAGS="--jobs=$(nproc)"

DEBUG_CFLAGS="-g"
DEBUG_CXXFLAGS="$DEBUG_CFLAGS"


### BUILD

BUILDENV=(!distcc color !ccache check !sign)


### OPTIONS

OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug lto !autodeps)
INTEGRITY_CHECK=(sha1)

STRIP_BINARIES="--strip-all"
STRIP_SHARED="--strip-unneeded"
STRIP_STATIC="--strip-debug"

MAN_DIRS=({usr{,/local}{,/share},opt/*}/{man,info})
DOC_DIRS=(usr/{,local/}{,share/}{doc,gtk-doc} opt/*/{doc,gtk-doc})
LIB_DIRS=('lib:usr/lib' 'lib32:usr/lib32')

PURGE_TARGETS=(usr/{,share}/info/dir .packlist *.pod)
DBGSRCDIR="/usr/src/debug"


### OUTPUT

PACKAGER="${USER} <@${HOSTNAME}>"


### COMPRESSION

COMPRESSBZ2=(bzip2 --stdout --force)
COMPRESSGZ=(gzip --to-stdout --force --no-name)
COMPRESSLZ=(lzip --stdout --force)
COMPRESSLZ4=(lz4 --quiet)
COMPRESSLRZ=(lrzip -q)
COMPRESSLZO=(lzop --quiet)
COMPRESSXZ=(xz --to-stdout --compress -)
COMPRESSZ=(compress -c -f)
COMPRESSZST=(zstd --stdout --compress --threads=0 -)


### EXTENSIONS

PKGEXT='.pkg.tar.zst'
SRCEXT='.src.tar.zst'
