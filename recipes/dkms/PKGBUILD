#! /bin/bash

pkgname="dkms"
epoch=100
arch=(any)

pkgdesc="Allows building and adding modules to any Linux kernel"
url="https://archlinux.org/packages/extra/any/dkms"


DotFiles=(
	.BUILDINFO
	.INSTALL
	.MTREE
	.PKGINFO
)


Version="$(
	curl --silent "${url}/" |
	grep '<title>Arch Linux -' |
	cut --delimiter=' ' --fields=9
)"


pkgver="$(
	echo "${Version}" |
	cut --delimiter='-' --fields=1
)"


pkgrel="$(
	echo "${Version}" |
	cut --delimiter='-' --fields=2
)"


RemoteRecipe="$(
	curl --silent "https://raw.githubusercontent.com/archlinux/svntogit-packages/packages/dkms/trunk/PKGBUILD"
)"


RemoteVar () {
	local Variable="${1}"
	local Position="${2}"

	echo "${RemoteRecipe}" |
	grep "${Variable}=" |
	head -n"${Position}" |
	tail -n1 |
	cut --delimiter='=' --fields=2 |
	tr --delete '()' |
	tr --delete '\n'
}


ImportRemoteArray () {
	local Name="${1}"
	local Position="${2}"

	readarray -td ' ' "${Name}" < <(RemoteVar "${Name}" "${Position}")
}


ImportRemoteArray "license" 1
ImportRemoteArray "depends" 1
readarray -t depends < <(
	echo "${depends[@]}" |
	tr -d "'" |
	tr " " "\n" |
	sort
)


source=(
	"${pkgname}-${pkgver}-${pkgrel}::${url}/download"
)


sha256sums=(
	SKIP
)


package () {
	tar --extract \
		--file "${srcdir}/${pkgbase}-${pkgver}-${pkgrel}" \
		--directory "${pkgdir}"

	cd "${pkgdir}"
	rm "${DotFiles[@]}"
}
